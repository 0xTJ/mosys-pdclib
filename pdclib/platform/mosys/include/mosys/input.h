#pragma once

#include <asm/ioctl.h>
#include <stdint.h>
#include <sys/time.h>

#define INPUT_MAGIC 'E'

struct input_event {
    struct timeval time;
    uint16_t type;
    uint16_t code;
    uint32_t value;
};
