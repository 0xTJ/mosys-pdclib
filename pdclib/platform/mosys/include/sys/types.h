#ifndef _INCLUDE_SYS_TYPES_H
#define _INCLUDE_SYS_TYPES_H

#ifndef _INO_T
#define _INO_T
typedef unsigned long ino_t;
#endif

#ifndef _MODE_T
#define _MODE_T
typedef unsigned short mode_t;
#endif

#ifndef _OFF_T
#define _OFF_T
typedef long off_t;
#endif

#ifndef _PID_T
#define _PID_T
typedef int pid_t;
#endif

#ifndef _SIZE_T
#define _SIZE_T
typedef unsigned long size_t;
#endif

#ifndef _SSIZE_T
#define _SSIZE_T
typedef long ssize_t;
#endif

#ifndef _UID_T
#define _UID_T
typedef unsigned int uid_t;
#endif

#endif
