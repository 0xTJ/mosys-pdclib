#ifndef _INCLUDE_SYS_MOUNT_H
#define _INCLUDE_SYS_MOUNT_H

int mount(const char *source, const char *target,
          const char *filesystemtype, unsigned long mountflags,
          const void *data);

#endif
