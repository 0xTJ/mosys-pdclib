cmake_minimum_required(VERSION 3.19)
project(PDCLib ASM C)

set(CMAKE_C_STANDARD 11)
set(CMAKE_C_STANDARD_REQUIRED True)

add_compile_options(-Wall -Wextra -pedantic)
add_compile_options(-O2)
add_compile_options(-Wno-unused-parameter)
add_compile_options(-fPIC -msep-data)
add_link_options(-pic)

set(DLMALLOC_C_FLAGS "-DLACKS_SCHED_H -DLACKS_SYS_PARAM_H -DLACKS_TIME_H -DHAVE_MORECORE=0")
set(PDCLIB_C_FLAGS "-D_PDCLIB_BUILD -D__STDC_NO_THREADS__ -D_PDCLIB_STATIC_DEFINE ${DLMALLOC_C_FLAGS}")

add_definitions("${PDCLIB_C_FLAGS}")

set(MOSYS_PDCLIB_PLATFORM "${CMAKE_SOURCE_DIR}/pdclib/platform/mosys")
file(GLOB_RECURSE OVERLAY_FILES
    "${MOSYS_PDCLIB_PLATFORM}/include/*"
    "${MOSYS_PDCLIB_PLATFORM}/functions/*"
    "${MOSYS_PDCLIB_PLATFORM}/CMakeLists.txt")

add_custom_command(OUTPUT "${CMAKE_SOURCE_DIR}/notmade"
    COMMAND ${CMAKE_COMMAND} -E true)
foreach(THIS_OVERLAY_FILE ${OVERLAY_FILES})
    file(RELATIVE_PATH THIS_OVERLAY_DIR "${MOSYS_PDCLIB_PLATFORM}" "${THIS_OVERLAY_FILE}")
    add_custom_command(OUTPUT "${CMAKE_SOURCE_DIR}/notmade"
        COMMAND ${CMAKE_COMMAND} -E copy_if_different
            ${THIS_OVERLAY_FILE}
            "${CMAKE_CURRENT_SOURCE_DIR}/${THIS_OVERLAY_DIR}"
        APPEND)
endforeach()
add_custom_target(pdclib_overlay ALL DEPENDS "${CMAKE_SOURCE_DIR}/notmade")

file(GLOB_RECURSE ASM_SOURCES "${CMAKE_CURRENT_SOURCE_DIR}/functions/*.s")
file(GLOB_RECURSE C_SOURCES "${CMAKE_CURRENT_SOURCE_DIR}/functions/*.c")

add_library(c ${ASM_SOURCES} ${C_SOURCES})

add_dependencies(c pdclib_overlay)

target_compile_options(c
    PRIVATE -Wno-format -Wno-type-limits)

file(GLOB_RECURSE PUBLIC_HEADER_FILES
    "${CMAKE_CURRENT_SOURCE_DIR}/include/*")
file(GLOB_RECURSE PRIVATE_HEADER_FILES
    "${CMAKE_CURRENT_SOURCE_DIR}/include/pdclib/*")

target_sources(c PUBLIC
    FILE_SET public_headers
    TYPE HEADERS
    BASE_DIRS "${CMAKE_CURRENT_SOURCE_DIR}/include"
    FILES "${PUBLIC_HEADER_FILES}")
target_sources(c PRIVATE
    FILE_SET private_headers
    TYPE HEADERS
    BASE_DIRS "${CMAKE_CURRENT_SOURCE_DIR}/include/pdclib"
    FILES "${PRIVATE_HEADER_FILES}")

include(GNUInstallDirs)
install(TARGETS c FILE_SET public_headers)
