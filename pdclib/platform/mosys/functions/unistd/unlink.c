#include <unistd.h>

#include <sys/syscall.h>

int unlink(const char *pathname) {
    return syscall(SYS_unlink, pathname);
}
