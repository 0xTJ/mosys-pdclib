#include <sys/types.h>
#include <unistd.h>

#include <sys/syscall.h>

off_t lseek(int fd, off_t offset, int whence) {
    return syscall(SYS_lseek, fd, offset, whence);
}
