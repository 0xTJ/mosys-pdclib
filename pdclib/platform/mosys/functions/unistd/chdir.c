#include <unistd.h>

#include <sys/syscall.h>

int chdir(const char *path) {
    return syscall(SYS_chdir, path);
}
