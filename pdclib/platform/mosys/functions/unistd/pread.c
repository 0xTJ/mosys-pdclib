#include <unistd.h>

#include <sys/syscall.h>

ssize_t pread(int fildes, void *buf, size_t nbyte, off_t offset) {
    return syscall(SYS_pread, fildes, buf, nbyte, offset);
}
