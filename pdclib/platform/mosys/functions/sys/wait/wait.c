#include <sys/types.h>
#include <sys/wait.h>

#include <unistd.h>
#include <sys/syscall.h>

pid_t wait(int *wstatus) {
    return waitpid(-1, wstatus, 0);
}

pid_t waitpid(pid_t pid, int *wstatus, int options) {
    return syscall(SYS_waitpid, pid, wstatus, options);
}
