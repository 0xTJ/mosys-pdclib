#include <sys/types.h>
#include <sys/socket.h>

#include <sys/syscall.h>

int socket(int domain, int type, int protocol) {
    return syscall(SYS_socket, domain, type, protocol);
}
