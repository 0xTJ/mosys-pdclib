#include <sys/mount.h>

#include <unistd.h>
#include <sys/syscall.h>

int mount(const char *source, const char *target,
          const char *filesystemtype, unsigned long mountflags,
          const void *data) {
    return syscall(SYS_mount, source, target, filesystemtype, mountflags, data);
}
