#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#include <unistd.h>
#include <sys/syscall.h>

int open(const char *path, int oflag, ...) {
    // TODO: Add other arguments
    return syscall(SYS_open, path, oflag, 0);
}
